from plinth import views


class MsmtpAppView(views.AppView):
    """Serve configuration page."""
    app_id = 'msmtp'
