## Short documentation of application task for GSoC21

The `msmtp` modules provides an interface for other FreedomBox modules
in order to be able to send notification emails to the admin of FreedomBox. 

It is marked using the `is_essential` field so that it gets installed by default,
but the admin must take care of the configuration file before it can properly send emails.

Because of an issue I encountered, 
which probably had to do with [this](https://marlam.de/msmtp/news/msmtp-permission-denied-disable-apparmor/) bug, 
I did not use the `msmtp`'s default configuration file, 
`.msmtprc` but created my own named`fbx.msmtp.conf.json` under `/etc` or `plinth/modules/msmtp/data/etc`. 
It has one field called `options` which consists of an array of command line options to be passed to `msmtp`.

Here is an example of `fbx.msmtp.conf.json`:

```
{
  "options": [
    "--host=smtp-mail.outlook.com",
    "--port=587",
    "--auth=on",
    "--user=example.user@hotmail.com",
    "--passwordeval=echo 'example-password'",
    "--tls",
    "--tls-starttls",
    "--from=example-user@hotmail.com"
  ]
}
```

There are better ways in order to avoid saving the password in cleartext in the configuration file, 
but for the sake of simplicity I left it as is here.

In the module's `__init__.py` file there is a `send_mail` function which can be used from other modules 
to send emails. It uses the `send-mail` action I created for msmtp which does the following:
 1. First it creates a temp file called `.fbx.msmtp.temp` in the form of 

```
From: <from-address>
To: <to-address>
Subject: <email subject>

<...Email body...>
```

 2. Then it uses the following command to actually send the mail.
   `<option-1>`, `<option-2>`, ... are the options defined in `fbx.msmtp.conf.json`.

```
cat .fbx.msmtp.temp | msmtp <to-address> <option-1> <option-2> <option-3> ...
```

I chose the prefix `fbx` for the configuration and the temp file
in order to avoid any potential similarities between files the `msmtp` package uses.

