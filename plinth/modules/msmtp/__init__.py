from plinth import app as app_module, actions

is_essential = True
managed_packages = ['msmtp']
version = 1.2


class MsmtpApp(app_module.App):
    """FreedomBox app for msmtp."""

    app_id = 'msmtp'

    def __init__(self):
        """Create components for the app."""
        super().__init__()

        info = app_module.Info(app_id=self.app_id,
                               version=version,
                               is_essential=is_essential,
                               name='msmtp',
                               short_description='smtp client')
        self.add(info)


def setup(helper, old_version=None):
    """Install and configure the module."""

    # Install necessary packages
    helper.allow_install = True
    helper.install(managed_packages)

    # Compose a test notification mail
    to_address = 'd1mike@hotmail.com'
    subject = "FreedomBox Notification: msmtp intalled successfully!"
    body = """Hello,

Your FreedomBox can now send notification emails!

Thank you for choosing FreedomBox!
"""

    # Send mail
    try:
        send_mail(to_address, subject, body)
    except ValueError:
        pass


def send_mail(to_address=None, subject=None, body=None):
    """Sends mail using the msmtp configuration file fbx.msmtp.conf.json
     defined either in /etc/ or in plinth/modules/msmtp/data/etc/"""

    if to_address is None or subject is None or body is None:
        raise ValueError('At least one null argument!')

    actions.superuser_run('msmtp', ['send-mail',
                                    '--to-address', to_address,
                                    '--subject', subject,
                                    '--body', body])
